
import React from 'react';
import { BrowserRouter} from 'react-router-dom';
import './App.css';
import GlobalStyle from './components/css/GlobalStyle';
import Content from './components/content';


function App() {
  return (
    <>
        <GlobalStyle />
        <div id="my-react-app">
            <BrowserRouter>
            
                <Content/>
            </BrowserRouter>
        </div>        
    </>     
  );
}

export default App;
