import React from "react";
import { Switch, Route } from 'react-router-dom';

import Home from "./Home";
import LogIn from "./LogIn";
import SignUp from "./SignUp";

//mport { AuthProvider } from "./Auth";


function Content (){
  return (
    <div id="site-content">
      <div className="container">
          <div className="row">
              <div className="col-12">
                  <div id="content">
                  
                
                      
                        <Switch>
                          <Route exact path="/home" component={Home} />
                          <Route exact path="/login" component={LogIn} />
                          <Route exact path="/" component={SignUp} />                        
                        </Switch>
                      
                  
                                   
                  </div>
              </div>
          </div>
      </div>
    </div>
  );
}
export default Content;