/* eslint-disable no-restricted-syntax */
/* eslint-disable no-console */

import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import FormLeftColumn from './formLeftContent';
// import  FormValidation from './FormValidation';
import { 
    FormWrapper, FormTitle, InputGroup, Label, Input, SubmitButton, FormError,
    RedirectLink, RedirectText
} from "./css/style";


const LogIn = () => {

  const history = useHistory();
  const [formState, setFormState] = useState({

      email: "",
      password: ""
  })
  const [errors, setErrors] = useState([]); 
  const [error, setError] = useState(""); 

  const redirectToHome = () => {
    history.push('/home');
  }

  const redirectToregister = () => {
      history.push('/');
  }
 
  const handleInputChange = (e) => {

      // const name = e.target.name;
      // const value = e.target.value;

      setFormState({
          ...formState, [e.target.name]: e.target.value,
      });

  }
 
    const FormValidation = (userInput) => {

      const formError = {};

      // checking email            
      const ValidEmailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/;

      if (!userInput.email) {
          formError.email = "Email should not be empty...";
      }else if (!ValidEmailFormat.test(String(userInput.email).toLowerCase())) {
          formError.email = "Invalid Email. Please enter valid email...";
      }else{
          formError.email = "";            
      }
      // checking password        
      if (!userInput.password) {
          formError.password = "Password should not be empty...";
      }else if (userInput.password.length < 4) {
          formError.password = "Password should be at least 4 characters...";
      }
      else{
          formError.password = "";            
      }   

      return formError;
    };


    const handleSubmitClick = async (e) => {
      e.preventDefault();

      const inputError = FormValidation(formState);

      // console.log(inputError);

      setErrors(inputError);

      // console.log(errors);

      if (!inputError.email &&  !inputError.password ) {

          await axios.get("https://test-user-form-dev-default-rtdb.firebaseio.com/UserDetail.json")          
            .then((response)=> {
              console.log(response);
              const getUserData = [];
              for (const key in response.data) {
                if (response.data) {
                  getUserData.push({ ...response.data[key], id: key });
                }
              } 
              // console.log(getUserData);

              getUserData.forEach((data) => {
                if (data.email === formState.email && data.password === formState.password) {
                  console.log("Logged In Success");
                  // setCurrentUser(userDetailValue);
                  redirectToHome();
                } else {
                  setError("Invalid details... Please Enter Valid Details...");
                }
              });

            });
        }else {
            console.log("Please Enter Valid Details");
        }
    };

    return (
      <>
          <div className="user-form-wrapper">
              <div className="row">
                  <div className="col-12 col-md-5">
                      <FormLeftColumn/>
                  </div>
                  <div className="col-12 col-md-7">
                      <FormWrapper>
                          <FormTitle>
                              <h4>Log In</h4>
                          </FormTitle>
                          <form>

                              <InputGroup>
                                  <Label>Email</Label>
                                  <Input
                                      name="email"
                                      type="email"
                                      placeholder="Please Enter Your Email Address"
                                      value={formState.email}
                                      onChange={handleInputChange} />
                              </InputGroup>
                              {errors.email ? <FormError>{errors.email}</FormError> : ""}
                              <InputGroup>
                                  <Label>Password</Label>
                                  <Input
                                      name="password"
                                      type="password"
                                      placeholder="Please Enter Your Password"
                                      value={formState.password}
                                      onChange={handleInputChange} />
                              </InputGroup>
                              {errors.password ? <FormError>{errors.password}</FormError> : ""}

                              <SubmitButton type="submit" onClick={handleSubmitClick}>LogIn</SubmitButton>
                          </form>

                          {error ? <FormError><p>{error}</p></FormError> : ""} 

                          <span className="small">OR</span>

                          <RedirectLink><span>Do not have an account </span>
                              <RedirectText onClick={() => redirectToregister()}>Register Here</RedirectText>
                          </RedirectLink> 

                      </FormWrapper>
                    </div>
                </div>
            </div>
        </>


    )

};

export default LogIn;

