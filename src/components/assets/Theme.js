
const Theme = {

    // color Declaration
    $baseColor: "#353535",
    $secondaryColor: "#008080",
    $colorSkyblue: "#007bff",
    $whiteColor: "#ffffff",
    $whiteSmoke: "#f7f7f7",

}

export default Theme
