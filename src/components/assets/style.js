import styled from "styled-components";
import Theme from "./Theme";

import DeviceSize from "./DeviceSize";

export const FormWrapper = styled.div`
  margin: 0 auto;
  text-align: center;
  border-radius: 10px;
  padding: 20px;
  background:${ Theme.$whiteColor };
`;

export const FormLeftContent = styled.div`
  background:${ Theme.$secondaryColor };
  text-align: center;
  color: #ffffff;
  height: 100%; 

  @media ${ DeviceSize.mobileS } {
    border-radius: 5px 5px 0 0px;
    padding:20px;    
  }
  @media ${ DeviceSize.tablet } {
    border-radius: 5px 0 0 5px;

  }     
  h2{
    margin-bottom:20px;
    font-weight:bold;
  }
`;

export const FormTitle = styled.div`
  h4{
    font-weight:bold;
  }
`;

export const InputGroup = styled.div`
  margin-bottom: 20px;
  text-align: left;
`;

export const Label = styled.label`
  text-transform:capitalize;
`;

export const Input = styled.input`
  padding: 5px 10px;
  width: 100%;
  background:${ Theme.$whiteSmoke };
  border-radius: 5px;
  border: 0;
`;

export const SubmitButton = styled.button`
  width: 100%;
  padding: 10px 20px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  margin-top: 10px;
  background:${ Theme.$secondaryColor };
  color:${ Theme.$whiteColor };

  &:hover{
    background:${ Theme.$colorSkyblue };
    color:${ Theme.$whiteColor };    
  }
`;

export const FormError = styled.div`
  margin: 20px 0;
  text-align: center;
  color: red;
`;

export const RedirectLink = styled.div`
  margin-top: 20px;
  cursor: pointer;
`;

export const RedirectText = styled.span`
  color:${ Theme.$colorSkyblue };
  font-weight: bold;
`;
