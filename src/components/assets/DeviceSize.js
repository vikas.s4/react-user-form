const size = {
    mobileS: '320px',
    mobileM: '375px',
    tablet: '768px',
    desktop: '992px',
    desktopL: '1200px'
}
  
export const DeviceSize= {
    mobileS: `(min-width: ${size.mobileS})`,
    mobileM: `(min-width: ${size.mobileM})`,
    tablet: `(min-width: ${size.tablet})`,
    desktop: `(min-width: ${size.desktop})`,
    desktopL: `(min-width: ${size.desktopL})`
  };
  