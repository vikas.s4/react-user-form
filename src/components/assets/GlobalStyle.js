import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`

    body{
        font-family: "Roboto", sans-serif;
        font-size: 14px;
        width: 100%;
        background-color: #f5f5f5;
    }    
`;
export default GlobalStyle;
