import React from "react";
import { Link } from "react-router-dom";


const Home = () =>{
  return (
    <div className="text-center">
        <h2 >Welcome To Home page</h2>
        
        <div className="text-center"><Link to="/">Create new account</Link></div>
                
    </div>
  );
};

export default Home;
