/* eslint-disable no-console */
/* eslint-disable consistent-return */
/* eslint-disable arrow-body-style */

import React, { useState } from "react";
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import FormLeftColumn from './formLeftContent';
// import  FormValidation from './FormValidation';
import { 
    FormWrapper, FormTitle, InputGroup, Label, Input, SubmitButton, FormError,
    RedirectLink, RedirectText
} from "./css/style";

function SignUp() {

    const history = useHistory();
    const [formState, setFormState] = useState({
        username: "",
        email: "",
        password: "",
        confirmPassword: ""
    })
    const [errors, setErrors] = useState([]); 
    const [error, setError] = useState(""); 

    const redirectToLogin = () => {
        history.push('/login');
    }

    const handleInputChange = (e) => {

        // const name = e.target.name;
        // const value = e.target.value;

        setFormState({
            ...formState, [e.target.name]: e.target.value,
        });

    }

    const FormValidation = (userInput) => {

        const formError = {};
        // checking name
        if (!userInput.username) {
            formError.username = "Name should not be empty...";
        }else{
            formError.username = "";            
        }
        // checking email            
        const ValidEmailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:.[a-zA-Z0-9-]+)*$/;

        if (!userInput.email) {
            formError.email = "Email should not be empty...";
        }else if (!ValidEmailFormat.test(String(userInput.email).toLowerCase())) {
            formError.email = "Invalid Email. Please enter valid email...";
        }else{
            formError.email = "";            
        }
        // checking password        
        if (!userInput.password) {
            formError.password = "Password should not be empty...";
        }else if (userInput.password.length < 4) {
            formError.password = "Password should be at least 4 characters...";
        }
        else{
            formError.password = "";            
        } 
        // checking confrimed password
        if (!userInput.confirmPassword) {
            formError.confirmPassword = "Confirmed Password should not be empty...";
        }else{
            formError.confirmPassword = "";            
        }
        // checking password equality
        if (userInput.password !== userInput.confirmPassword) {
            formError.password = "Password do not match"; 
        }   

        return formError;
    };

    const handleSubmitClick = async (e) => {
        e.preventDefault();

        const inputError = FormValidation(formState);

        // console.log(inputError);

        setErrors(inputError);

        console.log(errors);

        if (!inputError.username && !inputError.email &&  
            !inputError.password && !inputError.confirmPassword) {
 
                const SendFormData = {
                    "name": formState.username,
                    "email": formState.email,
                    "password": formState.password
                }
                await axios.post("https://test-user-form-dev-default-rtdb.firebaseio.com/UserDetail.json", SendFormData)
                    .then((response) => {
                        console.log(response);
                        if(response.status === 200){             
                            redirectToLogin();
                        } else{
                            // console.log("Some error occurred... Please Try Again Later...");
                            setError("Some error occurred... Please Try Again Later...");
                        }
                    })
                    .catch(() => {
                        console.log("Some error occurred... Please Try Again Later...");
                        setError("Some error occurred... Please Try Again Later...");
                    })

        }else {
            console.log("Please Enter Valid Details");
        }
    }

    return (
        <>
            <div className="user-form-wrapper">
                <div className="row">
                    <div className="col-12 col-md-5">
                        <FormLeftColumn/>
                    </div>
                    <div className="col-12 col-md-7">
                        <FormWrapper>
                            <FormTitle>
                                <h4>Register</h4>
                            </FormTitle>
                            <form>
                                <InputGroup>
                                    <Label>Name</Label>
                                    <Input
                                        name="username"
                                        type="text"
                                        placeholder="Please Enter Your Name"
                                        value={formState.username}
                                        onChange={handleInputChange} />
                                </InputGroup>
                                {errors.username ? <FormError>{errors.username}</FormError> : ""}
                                <InputGroup>
                                    <Label>Email</Label>
                                    <Input
                                        name="email"
                                        type="email"
                                        placeholder="Please Enter Your Email Address"
                                        value={formState.email}
                                        onChange={handleInputChange} />
                                </InputGroup>
                                {errors.email ? <FormError>{errors.email}</FormError> : ""}
                                <InputGroup>
                                    <Label>Password</Label>
                                    <Input
                                        name="password"
                                        type="password"
                                        placeholder="Please Enter Your Password"
                                        value={formState.password}
                                        onChange={handleInputChange} />
                                </InputGroup>
                                {errors.password ? <FormError>{errors.password}</FormError> : ""}
                                <InputGroup>
                                    <Label>Confirmed Password</Label>
                                    <Input
                                        name="confirmPassword"
                                        type="password"
                                        placeholder="Confirm Password"
                                        value={formState.confirmPassword}
                                        onChange={handleInputChange} />
                                </InputGroup>
                                {errors.confirmPassword ? <FormError>{errors.confirmPassword}</FormError> : ""}
                                <SubmitButton type="submit" onClick={handleSubmitClick}>Register</SubmitButton>
                            </form>
                            {error ? <FormError><p>{error}</p></FormError> : ""} 
                            <span className="small">OR</span>
                            <RedirectLink><span>Already have an account? </span>
                                <RedirectText onClick={() => redirectToLogin()}>Login here</RedirectText>
                            </RedirectLink>
                        </FormWrapper>
                    </div>
                </div>
            </div>
        </>


    )

};

export default SignUp;
