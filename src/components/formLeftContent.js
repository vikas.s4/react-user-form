import React from "react";
import { 
  FormLeftContent
} from "./assets/style";

function FormLeftColumn() {
  return (
    <FormLeftContent className="d-flex flex-column justify-content-center">
        <h2>Welcome To ReactJS</h2>
        <p>React Simple Registration and Login App using Firebase Real Time Database.</p>
    </FormLeftContent>
  );
}
export default FormLeftColumn;